
class WebServer < Apex::Server
  port 8080 # defaults to 8080

  layout do
    "<html>" +
        "<head><title>Apex</title></head>" +
        "<body>" +
        content +
        "</body>" +
        "</html>"
  end

  catch_all_requests do |r|
    "<h1>It works!</h1>" +
        "Request path: #{r.path}<br>" +
        "Request query: #{r.query}<br>" +
        "Request method: #{r.method}<br>" +
        "Request method: #{r.raw}<br>"

    p r.raw
  end

  # post "/some_post" do |request|
  #   request.headers["User-Agent"]
  # end
end

